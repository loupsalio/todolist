﻿
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using todolist;

namespace UnitTesttodolist
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GenerateTasks()
        {
            var tmp_date = DateTimeOffset.Now;
            var tmp = new my_task("0", "test title", "test description", tmp_date);

            Assert.AreEqual(tmp.id_, "0");
            Assert.AreEqual(tmp.title_, "test title");
            Assert.AreEqual(tmp.content_, "test description");
            Assert.AreEqual(tmp.time_, tmp_date);
            Assert.IsFalse(tmp.status_);
        }

        [TestMethod]
        public void ListTasksTest()
        {
            var tmp = new List<my_task>();

            tmp.Add(new my_task("0", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("1", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("2", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("3", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("4", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("5", "test title", "test description", DateTime.Now));
            Assert.AreEqual(tmp.Count, 6);
        }

        [TestMethod]
        public void checkedTest()
        {
            var tmp = new List<my_task>();

            tmp.Add(new my_task("0", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("1", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("2", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("3", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("4", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("5", "test title", "test description", DateTime.Now));
            foreach (var element in tmp)
            {
                element.status_ = true;
            }
            foreach (var element in tmp)
            {
                Assert.IsTrue(element.status_);
            }
        }

        [TestMethod]
        public void saveJson()
        {
            var tmp = new List<my_task>();

            tmp.Add(new my_task("0", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("1", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("2", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("3", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("4", "test title", "test description", DateTime.Now));
            tmp.Add(new my_task("5", "test title", "test description", DateTime.Now));

            var txt = JsonConvert.SerializeObject(tmp);
            tmp = JsonConvert.DeserializeObject<List<my_task>>(txt);

            Assert.AreEqual(tmp.Count, 6);
        }
    }
}
