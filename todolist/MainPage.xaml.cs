﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace todolist
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public List<my_task> master_task_list = new List<my_task>();

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Deserialize()
        {
            try
            {
                var storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                var sampleFile = await storageFolder.GetFileAsync("todolist.json");
                var text = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
                Debug.WriteLine(storageFolder.Path);

                master_task_list = JsonConvert.DeserializeObject<List<my_task>>(text) ?? new List<my_task>();
            }
            catch
            {
                Debug.WriteLine("Still empty");
            }
        }

        private void CalendarView_CalendarViewDayItemChanging(CalendarView sender, CalendarViewDayItemChangingEventArgs args)
        {
            var yesterday = DateTimeOffset.Now;
//            yesterday = yesterday.AddDays(-1);

            if (args.Item == null) return;
            DateTimeOffset focusedDate = args.Item.Date.Date;

            if (args.Phase == 0)
                args.RegisterUpdateCallback(CalendarView_CalendarViewDayItemChanging);
           /* else if (args.Phase == 1)
            {
                args.Item.IsBlackout = args.Item.Date < yesterday;
                args.RegisterUpdateCallback(CalendarView_CalendarViewDayItemChanging);
            }*/
            else if (args.Phase == 1)
            {

                var densityColors = new List<Color>();
                foreach (my_task element in master_task_list)
                {
                    if (element.time_.Day != focusedDate.Day || element.time_.Month != focusedDate.Month ||
                        element.time_.Year != focusedDate.Year) continue;
                    if (element.status_ == true)
                    {
                        densityColors.Add(Colors.Green);
                    }
                    else
                    {
                        densityColors.Add(args.Item.Date < yesterday ? Colors.Red : Colors.Blue);
                    }
                }
                args.Item.SetDensityColors(densityColors);
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Deserialize();
            base.OnNavigatedTo(e);
        }

        private void change_focus_date(object sender, CalendarViewSelectedDatesChangedEventArgs e)
        {
            this.Frame.Navigate(typeof(TaskPage), my_calendar.SelectedDates.First());
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
    }

}
