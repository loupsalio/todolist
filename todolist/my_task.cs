﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todolist
{

    public class my_task
    {
        public string id_ { get; set; }
        public string title_ { get; set; }
        public string content_ { get; set; }
        public bool status_ { get; set; }
        public DateTimeOffset time_ { get; set; }
        public my_task(string id, string title, string content, DateTimeOffset time)
        {            
            id_ = id;
            time_ = time;
            title_ = title;
            content_ = content;
            status_ = false;
        }
    }
}
