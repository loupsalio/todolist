﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace todolist
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class TaskPage : Page
    {
        private List<my_task> master_tasks_list = new List<my_task>();
        private ObservableCollection<my_task> task_list = new ObservableCollection<my_task>();
        private JsonSerializer serializer = new JsonSerializer();
        private DateTimeOffset focused_date;

        public TaskPage()
        {
            this.InitializeComponent();
        }

        private async void Serialize()
        {

            var storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            var sampleFile = await storageFolder.CreateFileAsync("todolist.json",
                Windows.Storage.CreationCollisionOption.ReplaceExisting);
            Debug.WriteLine(storageFolder.Path);

            await Windows.Storage.FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(master_tasks_list));
        }

        private async void Deserialize()
        {

            try
            {

                var storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                var sampleFile = await storageFolder.GetFileAsync("todolist.json");
                Debug.WriteLine(storageFolder.Path);
                var text = await Windows.Storage.FileIO.ReadTextAsync(sampleFile);
                master_tasks_list = JsonConvert.DeserializeObject<List<my_task>>(text) ?? new List<my_task>();
                foreach (var element in master_tasks_list)
                {
                    if (element.time_.Day == focused_date.Day && element.time_.Month == focused_date.Month && element.time_.Year == focused_date.Year)
                        task_list.Add(element);
                }
                tasks_listbox.ItemsSource = task_list;
            }
            catch
            {
                Debug.WriteLine("Still empty");
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is DateTimeOffset)
            {
                focused_date = (DateTimeOffset)e.Parameter;
                Deserialize();
                title_date.Text = focused_date.Day.ToString() + "/" + focused_date.Month.ToString() + "/" + focused_date.Year.ToString();
            }
            else
            {
                this.Frame.Navigate(typeof(MainPage));
            }
            base.OnNavigatedTo(e);
        }

        private void Add_button_OnClick(object sender, RoutedEventArgs e)
        {
            var tmp = new my_task(master_tasks_list.Count.ToString(), name_task.Text, details_task.Text, focused_date);
            Debug.WriteLine(title_date.Text);
            master_tasks_list.Add(tmp);
            task_list.Add(tmp);
            tasks_listbox.ItemsSource = task_list;
            Serialize();
        }

        private void Remove_button_OnClick(object sender, RoutedEventArgs e)
        {
            if (tasks_listbox.SelectedItem == null)
                return;

            for (var i = 0; i < task_list.Count; i++)
            {
                if (tasks_listbox.SelectedItem == task_list[i])
                {
                    master_tasks_list.Remove(task_list[i]);
                    task_list.Remove(task_list[i]);
                    tasks_listbox.ItemsSource = task_list;
                    break;
                }


            }
            Serialize();
        }
        private void Back_button_OnClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void check_task(object sender, RoutedEventArgs e)
        {

            var checkbox = sender as CheckBox;
            if (checkbox.Content == null)
            {
                return;
            }
            var text = checkbox.Content.ToString();
            Debug.WriteLine(text);
            foreach (var task in task_list)
            {
                if (task.title_ == text && task.time_ == focused_date)
                {
                    task.status_ = true;
                    break;
                }
            }
            foreach (var task in master_tasks_list)
            {
                if (task.title_ == text && task.time_ == focused_date)
                {
                    task.status_ = true;
                    break;
                }
            }
            Serialize();
        }

        private void uncheck_task(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as CheckBox;
            var text = checkbox.Content.ToString();
            Debug.WriteLine(text);
            foreach (var task in task_list)
            {
                if (task.title_ == text && task.time_ == focused_date)
                {
                    task.status_ = false;
                    break;
                }
            }
            foreach (var task in master_tasks_list)
            {
                if (task.title_ == text && task.time_ == focused_date)
                {
                    task.status_ = false;
                    break;
                }
            }
            Serialize();
        }


    }
}
